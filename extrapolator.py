import tensorflow as tf
from io import BytesIO
from PIL import Image
import numpy as np
import sys

def read_image(filename):
    fd = None
    fd = tf.io.gfile.GFile(filename, 'rb')

    pil_image = Image.open(fd)
    width, height = pil_image.size
    # crop to make the image square
    pil_image = pil_image.crop((0, 0, height, height))
    pil_image = pil_image.resize((257,257),Image.ANTIALIAS)
    image_unscaled = np.array(pil_image)
    image_np = np.expand_dims(
        image_unscaled.astype(np.float32) / 255., axis=0)
    return image_np


input_img = read_image("/tmp/imageExtrapolator-{}.jpg".format(sys.argv[1]))

model_handle = "https://tfhub.dev/google/boundless/quarter/1"

model = tf.saved_model.load("./boundless_quarter_1")

result = model.signatures['default'](tf.constant(input_img))
generated_image =  result['default']

generated_image = np.squeeze(generated_image.numpy())
generated_image = (generated_image*255).astype("uint8")
Image.fromarray(generated_image).save("/tmp/imageExtrapolator-{}.jpg".format(sys.argv[1]))
#comment
