FROM debian:latest

RUN apt update

RUN apt -y install openjdk-17-jre python3 python3-pip

COPY . /bin

RUN pip3 install -r /bin/requirements.txt

ENV PORT 3000

CMD java -jar /bin/net.dulatello08.imageextrapolator-0.0.1-all.jar

